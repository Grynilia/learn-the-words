import HeaderBlock from "./components/HeaderBlock";
import React from "react";
import Paragraph from "./components/Paragraph";
import Header from "./components/Header";
import {ReactComponent as ReactLogo} from './logo.svg';
import Card from "./components/Card";
import './index.css';

export const wordsList = [
    {
        eng: 'between',
        rus: 'между'
    },
    {
        eng: 'high',
        rus: 'высокий'
    },
    {
        eng: 'really',
        rus: 'действительно'
    },
    {
        eng: 'something',
        rus: 'что-нибудь'
    },
    {
        eng: 'most',
        rus: 'большинство'
    },
    {
        eng: 'another',
        rus: 'другой'
    },
    {
        eng: 'much',
        rus: 'много'
    },
    {
        eng: 'family',
        rus: 'семья'
    },
    {
        eng: 'own',
        rus: 'личный'
    },
    {
        eng: 'out',
        rus: 'из/вне'
    },
    {
        eng: 'leave',
        rus: 'покидать'
    },
];


const App = () => {
    return(
        <>
            <HeaderBlock>
                <Header>
                Надо учить слова онлайн
                </Header>
                <Paragraph>
                    Используйте карточки для запоминания и пополняйте активный словарный запас.
                </Paragraph>
            </HeaderBlock>
            <div className='headerCenter'> Нажмите,чтобы узнать что за слово</div>
                <div className='textCenter'>
                    {
                        wordsList.map(({ eng, rus }, index) => (<Card key={index} eng={eng} rus ={rus} />))
                    }
                </div>
            <HeaderBlock hideBackground>
                <Header>
                    Еще один заголовок
                </Header>
                <Paragraph>
                    Ну здорово же!
                </Paragraph>
                <ReactLogo/>
            </HeaderBlock>
        </>
    );
}

export default App;