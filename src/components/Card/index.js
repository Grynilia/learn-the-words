import React from 'react';
import cl from 'classnames';
import s from './Card.module.scss';

class Card extends React.Component {

    state = {
        done: false,
    }

    handleCartClick = () => {
        this.setState({
            done: !this.state.done
        });
        console.log('###' , this.state);
    }

    render() {
        const {eng, rus} = this.props;
        const {done} = this.state;

        return (
            <div
                className={ cl(s.card, { [s.done]: done }) }
                onClick={this.handleCartClick}
            >
                <div className={s.cardInner}>
                    <div className={s.cardFront}>
                        { eng }
                    </div>
                    <div className={s.cardBack}>
                        { rus }
                    </div>
                </div>

            </div>
        );
    }
}
export default Card;
